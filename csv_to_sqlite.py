import sqlite3, csv
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn





def create_task(conn, to_db):
    """
    Create a new task
    :param conn:
    :param task:
    :return:
    """

    #sql = ''' INSERT INTO workouts(workout_name, primary_m_group, secondary_m_group, type_of_workout, rating, calorie_score)
              #VALUES(?,?,?,?,?,?) '''
    sql = ''' INSERT INTO workouts(workout_name, primary_m_group)
              VALUES(?,?) '''
    #cur = conn.cursor()
    #cur.execute(sql, task)
    #sql = "INSERT INTO test(Field2, Field3) VALUES(?,?,)"
    cur = conn.cursor()
    cur.executemany(sql, to_db)
    conn.commit()
    conn.close()
    return conn


def main():
    database = r"play.db"

    # create a database connection
    conn = create_connection(database)
    with conn:
        # create a new project
        #project = ('Cool App with SQLite & Python', '2015-01-01', '2015-01-30')
        #project_id = create_project(conn, project)

        # tasks
        #task_1 = ('sf','Lats', 'Biceps', 'Body-weight', '10', '10',)
        #task_2 = ('bbb', 'sfds', 'efefe', 'Body-weight', '10', '10',)
        #tasktest = ('a',1)
        #mylist = [task_1, task_2]
        #task_2 = ('Confirm with user about the top requirements', 1, 1, project_id, '2015-01-03', '2015-01-05')

        with open('exercise_file.csv','r') as fin:
            reader = csv.reader(fin, delimiter=',')

            for row in reader:
                to_db = [row[0], row[1]] # Appends data from CSV file representing and handling of text
                cur = conn.cursor()
                cur.execute("INSERT INTO workouts (workout_name, primary_muscle_group) VALUES(?, ?);", to_db)
            conn.commit()
            #conn.close() # closes connection to database


            #to_db = [(i['workout_name'], i['primary_m_group']) for i in dr]
        # create tasks
        #create_task(conn, to_db)
        #create_task(conn, task_2)


if __name__ == '__main__':
    main()